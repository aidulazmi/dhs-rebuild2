class PriceAnalysisController < ApplicationController
  
  def index
    @item = Item.new
  end
  # def get_price
  #   item = Item.find_by(
  #     type_id: params[:type_id],
  #     process_connection_type: params[:process_connection_type],
  #     size: params[:size],
  #     class_item: params[:class_item],
  #     rating: params[:rating],
  #     schedule: params[:schedule]
  #   )

  #   if item.present?
  #     price = PriceItem.find_by(item_id: item.id)&.price
  #     render json: { name: item.name, price: price }
  #   else
  #     render json: { error: 'Item not found' }, status: :not_found
  #   end
  # end
  # masirvan
  # def get_price
  #   type_id = params[:type_id]
  #   size = params[:size]
  #   class_item = params[:class_item]
  #   dimension = params[:dimension] # new parameter
  #   schedule = params[:schedule] # new parameter
  #   wall_thickness = params[:wall_thickness] # new parameter
    
  #   item = Item.find_by(type_id: type_id, size: size, class_item: class_item, dimension: dimension, schedule: schedule, wall_thickness: wall_thickness)
  
  #   if item.present?
  #     price_item = PriceItem.find_by(item_id: item.id)
  #     price = price_item&.price
  #     date = price_item&.date
  #     render json: { name: item, price: price, date: date }
  #   else
  #     render json: { error: 'Item not found' }, status: :not_found
  #   end
  # end

  # Tetsing
  def get_price
    type_id = params[:type_id]
    size = params[:size]
    class_item = params[:class_item]
    dimension = params[:dimension] # new parameter
    schedule = params[:schedule] # new parameter
    wall_thickness = params[:wall_thickness] # new parameter
      
    items = Item.where(type_id: type_id, size: size, class_item: class_item, dimension: dimension, schedule: schedule, wall_thickness: wall_thickness)
    
    if items.present?
      results = items.map do |item|
        price_item = PriceItem.find_by(item_id: item.id)
        { name: item, price: price_item&.price, date: price_item&.date }
      end
      render json: results
    else
      render json: { error: 'Item not found' }, status: :not_found
    end
  end

  

end
