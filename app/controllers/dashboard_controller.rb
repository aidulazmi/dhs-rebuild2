# Dashboard section
class DashboardController < ApplicationController
  include DashboardHelper
  # params[:disipline_id]

  def items_show
    @items = Item.left_outer_joins(:brand,:disipline,:type,:product,:vendor).where('items.id = ?', params[:id]).select("*, items.id AS iditems, vendors.name AS namevendors, disiplines.name AS namedisiplines").order('brands.name_brand ASC')
    @priceItems = PriceItem.left_outer_joins(:vendor,:currency).where(item_id: params[:id]).select('*, vendors.name AS namevendors, price_items.id AS idpriceitems')
    @vendorPrice = PriceItem.left_outer_joins(:vendor,:currency,:country).where(item_id: params[:id]).select('*, vendors.name AS namevendors, price_items.id AS idpriceitems').order('price_items.date DESC').limit(1)
    @itemss = DetailItem.left_outer_joins(:unit).where(item_id: params[:id]).select('*').order('units.unit_name DESC').limit(1)
  end

  def items_table
    @getDisipline = getDisiplineItemsTable
    @log_activities = Item.left_outer_joins(:brand,:disipline,:type,:product,:vendor).where(disipline_id: params[:disipline_id]).select('*, items.id AS iditems, vendors.name AS namevendors, disiplines.name AS namedisiplines').order("#{sort_column} #{sort_direction}, brands.name_brand")
    respond_to do |format|
      format.html
      # you can pass the view_context if you want to use helper methods\
      # @coba = "rinaldi.fauzi"
      # for parsing params from outside
      # format.json {render json: LogActivitiesDatatable.new(view_context, {coba: @coba})}
      #------------END---------------------
      format.json {render json: DashboardsItemsTable.new(view_context)}
    end
  end
  def home_piping

  end
  def home_pipeline
    @count = Item.where("id = ? AND document = ?", 1, "Insulation Joint").count

  end
  def home_instrument
    
  end
  def home_civil
    
  end
  def home_jargas
    
  end
  

  def items_search
    
    # @items = Item.left_outer_joins(:brand,:disipline,:type,:product,:vendor).where('general_spec LIKE ? or general_spec LIKE ?', "%#{params[:param].titleize}%", "%#{params[:param].downcase}%").select("*, items.id AS iditems, vendors.name AS namevendors, disiplines.name AS namedisiplines").order('brands.name_brand')
    # @items = Item.left_outer_joins(:brand,:disipline,:type,:product,:vendor).where('general_spec ILIKE ? or vendors.name ILIKE ?', "%#{params[:param].titleize}%", "%#{params[:param].downcase}%").select("*, items.id AS iditems, vendors.name AS namevendors, disiplines.name AS namedisiplines").order('brands.name_brand')
    # @items = Item.left_outer_joins(:brand,:disipline,:type,:product,:vendor).whsudo apere('general_spec ILIKE ? OR vendors.name ILIKE ?', "%#{params[:param]}%", "%#{params[:parjadi am]}%").select("*, items.id AS iditems, vendors.name AS namevendors, disiplines.name AS namedisiplines").order('brands.name_brand')
    @items = Item.left_outer_joins(:brand, :disipline, :type, :product, :vendor).where('general_spec ILIKE ? OR vendors.name ILIKE ? OR products.product_name ILIKE ? OR types.name_type ILIKE ? OR disiplines.name ILIKE ? OR brands.name_brand ILIKE ?', "%#{params[:param]}%", "%#{params[:param]}%", "%#{params[:param]}%", "%#{params[:param]}%", "%#{params[:param]}%", "%#{params[:param]}%").select("*, items.id AS iditems, vendors.name AS namevendors, disiplines.name AS namedisiplines, types.name_type AS nametypes").order('brands.name_brand')
  end

  def getItemsData
    respond_to do |format|
      format.html
      # you can pass the view_context if you want to use helper methods\
      # @coba = "rinaldi.fauzi"
      # for parsing params from outside
      # format.json {render json: LogActivitiesDatatable.new(view_context, {coba: @coba})}
      #------------END---------------------
      format.json {render json: DashboardDataBasetype.new(view_context)}
    end
  end

  def items_home
    
  end

  def analytics
  end

 def sort_column
    columns = %w[product_name name_type name_brand namedisiplines general_spec namevendors price]
    columns[params[:iSortCol_0].to_i]
  end
  def sort_direction
    params[:sSortDir_0] = "desc" ? "desc" : "asc"
  end
end
