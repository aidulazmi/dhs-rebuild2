class JargasInstrumentsController < ApplicationController
  before_action :set_item, only: [:show, :edit, :update, :destroy]
  
  def index
    @log_activities1 = Item.left_outer_joins(:brand, :disipline, :type, :product, :vendor, :price_item)
                         .where(disipline_id: 43, document: 'Jargas-Instrument')
                         .select('*, items.id AS iditems, vendors.name AS namevendors, disiplines.name AS namedisiplines')
                         .order("price_items.date DESC")
    respond_to do |format|
      format.html
      format.json {render json: ItemsIndex.new(view_context)}
    end
  end

  def getDataTemplate
    send_file 'lib/templatesDataItems/TemplateDHS/JARGAS - JASA KONSTRUKSI/Instrument.xlsx', :type => "application/xlsx", :x_sendfile => true
  end

    # GET /items/new
  def new
    @item = Item.new
  end

  def destroy
    @item.destroy
    respond_to do |format|
      format.html { redirect_to index_jargas_instruments_jargas_instrument_path(43), notice: 'Item was successfully destroyed.' }
      format.json { head :no_content }
    end
  end
  
  def items_show
    @items = Item.left_outer_joins(:brand,:disipline,:type,:product,:vendor).where('items.id = ?', params[:id]).select("*, items.id AS iditems, vendors.name AS namevendors, disiplines.name AS namedisiplines").order('brands.name_brand ASC')
    @priceItems = PriceItem.left_outer_joins(:vendor,:currency).where(item_id: params[:id]).select('*, vendors.name AS namevendors, price_items.id AS idpriceitems')
    @vendorPrice = PriceItem.left_outer_joins(:vendor,:currency,:country).where(item_id: params[:id]).select('*, vendors.name AS namevendors, price_items.id AS idpriceitems').order('price_items.date DESC').limit(1)
    @itemss = DetailItem.left_outer_joins(:unit).where(item_id: params[:id]).select('*').order('units.unit_name DESC').limit(1)
  end

  def edit
 
  end

  def create
    @item = Item.new(item_params)

    respond_to do |format|
      @getDisipline = Disipline.find(params[:item][:disipline_id])
      @getBrand = Brand.find(params[:item][:brand_id])
      @getType = Type.find(params[:item][:type_id])

      @arrayData = []
      @arrayData = {
          'Disipline : ' => @getDisipline ? @getDisipline.name : "",
          'Brand : ' => @getBrand ? @getBrand.name_brand : "",
          'Type : ' => @getType ? @getType.name_type : "",
          'General Spec : ' => params[:item][:general_spec]
      }
      setLogActivity("Input data with list : " + @arrayData.to_s)
      if @item.save
        @items = Item.last
        format.html { redirect_to new2_price_items_path(@items.id), notice: 'Item was successfully created.' }
        format.json { render :show, status: :created, location: @item }
      else
        format.html { render :new }
        format.json { render json: @item.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /items/1
  # PATCH/PUT /items/1.json
  def update
    @getItemChange = Item.find(params[:id])
    @getItemChange.general_spec = params[:item][:general_spec]
    setLogActivity("Item changed : " + "#{@getItemChange.changes}")
    respond_to do |format|
      if @item.update(item_params)
        format.html { redirect_to @item, notice: 'Item was successfully updated.' }
        format.json { render :show, status: :ok, location: @item }
      else
        format.html { render :edit }
        format.json { render json: @item.errors, status: :unprocessable_entity }
      end
    end
  end
  def delete_selected
    selected_items = params[:selected_items]
    selected_items.each do |item_id|
      item = Item.find(item_id)
      item.destroy
    end
    redirect_to index_jargas_instruments_jargas_instrument_path(43), notice: "Selected items have been deleted."
  end

  private

  def set_item
    @item = Item.find(params[:id])
  end

  # Never trust parameters from the scary internet, only allow the white list through.
  def item_params
    params.require(:item).permit(:date,:project_name,:delivery_time,:incoterm,:vat,:term_payment,:price,:currency_id, :vendor_id, :country_id, :unit_id, :product_id, :type_id, :brand_id, :disipline_id, :size, :class_item, :dimension, :general_spec, :scope_of_supply, :others, :note, :delivery_point, :schedule, :degree, :wall_thickness, :remark, :valve_specification, :actuator_specification, :solenoid_specification, :limit_swict_specification, :sensor, :housing_spec, :flowmeter_specification, :transmitter, :process_control_system_spec_2, :safety_instrumented_system_spec, :fire_and_gas_system_spec, :remote, :termina_unit_spec, :design_code, :material_ff_construction, :date2, :sch, :document, :no_quotation)
  end
  
  def sort_column
    columns = %w[name_brand namedisiplines name_type product_name size class_item dimension general_spec]
    columns[params[:iSortCol_0].to_i]
  end

  def sort_direction
    params[:sSortDir_0] = "desc" ? "desc" : "asc"
  end


end
