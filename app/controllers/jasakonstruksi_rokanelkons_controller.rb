class JasakonstruksiRokanelkonsController < ApplicationController

  def index
    @log_activities1 = Item.left_outer_joins(:brand,:disipline,:type,:product,:vendor).where(disipline_id: 42, document: 'Rokan Elkon').select('*, items.id AS iditems, vendors.name AS namevendors, disiplines.name AS namedisiplines').order("#{sort_column} #{sort_direction}, brands.name_brand")
    respond_to do |format|
      format.html
      format.json {render json: ItemsIndex.new(view_context)}
    end
  end

  def getDataTemplate
    send_file 'lib/templatesDataItems/TemplateDHS/JASA KONSTRUKSI/ROKANELKON.xlsx', :type => "application/xlsx", :x_sendfile => true
  end

  private

  def set_item
    @item = Item.find(params[:id])
  end

  # Never trust parameters from the scary internet, only allow the white list through.
  def item_params
    params.require(:item).permit(:date,:project_name,:delivery_time,:incoterm,:vat,:term_payment,:price,:currency_id, :vendor_id, :country_id, :unit_id, :product_id, :type_id, :brand_id, :disipline_id, :size, :class_item, :dimension, :general_spec, :scope_of_supply, :others, :note, :delivery_point, :schedule, :degree, :wall_thickness, :remark, :valve_specification, :actuator_specification, :solenoid_specification, :limit_swict_specification, :sensor, :housing_spec, :flowmeter_specification, :transmitter, :process_control_system_spec_2, :safety_instrumented_system_spec, :fire_and_gas_system_spec, :remote, :termina_unit_spec, :design_code, :material_ff_construction, :date2, :sch, :document)
  end
  
  def sort_column
    columns = %w[name_brand namedisiplines name_type product_name size class_item dimension general_spec]
    columns[params[:iSortCol_0].to_i]
  end

  def sort_direction
    params[:sSortDir_0] = "desc" ? "desc" : "asc"
  end
end
