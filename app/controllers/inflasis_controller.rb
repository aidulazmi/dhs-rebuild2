class InflasisController < ApplicationController
  before_action :set_inflasi, only: [:show, :edit, :update, :destroy]
  respond_to :html, :json
  def index
  @inflasis = Inflasi.all
end
  def new
    @inflasis = Inflasi.new
  end
  
  def show

  end

  def create
    @inflasis = Inflasi.new(inflasi_params)
    respond_to do |format|
      if params[:modalbox].present?
        if @inflasis.save
           #format.html { redirect_back(fallback_location: root_path, :notice => "Inflasi was successfully created.") }
           #format.json { render :show, status: :created, location: @inflasis }
           format.html { redirect_to inflasis_index_path}
        else
           #format.html { render :new }
           #format.json { render json: @inflasis.errors, status: :unprocessable_entity }
           format.html { redirect_to inflasis_index_path}
        end
      else
        if @inflasis.save
           #format.html { redirect_to @inflasis, notice: 'Insflasi was successfully created.' }
           #format.json { render :show, status: :created, location: @inflasis }
           format.html { redirect_to inflasis_index_path}
        else
           #format.html { render :new }
           #format.json { render json: @inflasis.errors, status: :unprocessable_entity }
          format.html { redirect_to inflasis_index_path}
        end
      end
    end
  end

  
  private

  # Use callbacks to share common setup or constraints between actions.
  def set_inflasi
    @inflasis = Inflasi.find(params[:id])
  end

  def inflasi_params
    params.require(:inflasi).permit(:bulan,:inflasi,:keterangan)
  end
end