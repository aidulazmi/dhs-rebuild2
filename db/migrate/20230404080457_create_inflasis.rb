class CreateInflasis < ActiveRecord::Migration[5.2]
  def change
    create_table :inflasis do |t|
      t.string : tanggal_inflasi
      t.string : inflasi
      t.string : keterangan
      t.timestamps
    end
  end
end
