# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 2023_04_10_035112) do

  # These are extensions that must be enabled in order to support this database
  enable_extension "plpgsql"

  create_table "brands", id: :serial, force: :cascade do |t|
    t.string "name_brand"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
  end

  create_table "brochures", id: :serial, force: :cascade do |t|
    t.string "brochure_title"
    t.string "brochure_path"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.integer "item_id"
    t.index ["item_id"], name: "index_brochures_on_item_id"
  end

  create_table "countries", id: :serial, force: :cascade do |t|
    t.string "country_code"
    t.string "country_name"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
  end

  create_table "currencies", id: :serial, force: :cascade do |t|
    t.string "current_name"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
  end

  create_table "deliveries", id: :serial, force: :cascade do |t|
    t.string "delivery_place"
    t.string "delivery_inconterm"
    t.date "delivery_date"
    t.integer "delivery_vat"
    t.string "delivery_term_payment"
    t.string "delivery_note"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
  end

  create_table "delivery_items", id: :serial, force: :cascade do |t|
    t.integer "delivery_id"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.integer "price_item_id"
    t.index ["delivery_id"], name: "index_delivery_items_on_delivery_id"
    t.index ["price_item_id"], name: "index_delivery_items_on_price_item_id"
  end

  create_table "detail_items", id: :serial, force: :cascade do |t|
    t.integer "unit_id"
    t.string "dimension2"
    t.string "dimension3"
    t.string "dft"
    t.string "vol_solid"
    t.string "spreading_rate"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.integer "item_id"
    t.index ["item_id"], name: "index_detail_items_on_item_id"
    t.index ["unit_id"], name: "index_detail_items_on_unit_id"
  end

  create_table "disiplines", id: :serial, force: :cascade do |t|
    t.string "name"
    t.string "category"
    t.string "logo"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
  end

  create_table "indonesia_cities", id: :serial, force: :cascade do |t|
    t.integer "province_id"
    t.string "name"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
  end

  create_table "indonesia_districts", id: :serial, force: :cascade do |t|
    t.integer "city_id"
    t.string "name"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
  end

  create_table "indonesia_provinces", id: :serial, force: :cascade do |t|
    t.string "name"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
  end

  create_table "indonesia_villages", id: :serial, force: :cascade do |t|
    t.integer "district_id"
    t.string "name"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
  end

  create_table "inflasis", force: :cascade do |t|
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.string "inflasi"
    t.string "keterangan"
    t.string "bulan"
    t.integer "tahun"
  end

  create_table "item_price_letters", id: :serial, force: :cascade do |t|
    t.integer "item_id"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.integer "price_item_id"
    t.index ["item_id"], name: "index_item_price_letters_on_item_id"
    t.index ["price_item_id"], name: "index_item_price_letters_on_price_item_id"
  end

  create_table "items", id: :serial, force: :cascade do |t|
    t.integer "brand_id"
    t.integer "disipline_id"
    t.string "size"
    t.string "class_item"
    t.string "dimension"
    t.text "general_spec"
    t.string "scope_of_supply"
    t.string "others"
    t.text "note"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.integer "type_id"
    t.integer "product_id"
    t.string "delivery_point"
    t.integer "vendor_id"
    t.string "schedule"
    t.string "degree"
    t.string "wall_thickness"
    t.string "remark"
    t.string "valve_specification"
    t.string "actuator_specification"
    t.string "solenoid_specification"
    t.string "limit_swict_specification"
    t.string "sensor"
    t.string "housing_spec"
    t.string "flowmeter_specification"
    t.string "transmitter"
    t.string "process_control_system_spec_2"
    t.string "safety_instrumented_system_spec"
    t.string "fire_and_gas_system_spec"
    t.string "remote"
    t.string "termina_unit_spec"
    t.string "design_code"
    t.string "material_ff_construction"
    t.string "date2"
    t.string "sch"
    t.string "no_quotation"
    t.string "delivery_time"
    t.string "document"
    t.string "process_connection_type"
    t.string "unit_id"
    t.index ["brand_id"], name: "index_items_on_brand_id"
    t.index ["disipline_id"], name: "index_items_on_disipline_id"
    t.index ["product_id"], name: "index_items_on_product_id"
    t.index ["type_id"], name: "index_items_on_type_id"
    t.index ["vendor_id"], name: "index_items_on_vendor_id"
  end

  create_table "piping-pipe", id: :integer, default: -> { "nextval('items_id_seq'::regclass)" }, force: :cascade do |t|
    t.integer "brand_id"
    t.integer "disipline_id"
    t.string "size"
    t.string "product_id"
    t.string "type_id"
    t.string "schedule"
    t.string "class"
    t.string "general_spec"
    t.string "unit_id"
    t.string "currency_id"
    t.string "coo_id"
    t.integer "vendor_id"
    t.string "delivery_point"
    t.string "delivery_time"
    t.string "incoterm"
    t.string "date"
    t.string "no_quotation"
    t.string "note"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "price_items", id: :serial, force: :cascade do |t|
    t.string "place"
    t.string "project_name"
    t.string "incoterm"
    t.date "date"
    t.string "vat"
    t.text "term_payment"
    t.text "delivery_time"
    t.float "price"
    t.integer "status"
    t.string "note"
    t.integer "last"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.integer "price_letter_id"
    t.integer "referensi_item_id"
    t.integer "country_id"
    t.integer "vendor_id"
    t.integer "currency_id"
    t.integer "item_id"
    t.index ["country_id"], name: "index_price_items_on_country_id"
    t.index ["currency_id"], name: "index_price_items_on_currency_id"
    t.index ["item_id"], name: "index_price_items_on_item_id"
    t.index ["price_letter_id"], name: "index_price_items_on_price_letter_id"
    t.index ["referensi_item_id"], name: "index_price_items_on_referensi_item_id"
    t.index ["vendor_id"], name: "index_price_items_on_vendor_id"
  end

  create_table "price_letters", id: :serial, force: :cascade do |t|
    t.string "price_letter_title"
    t.string "price_letter_path"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
  end

  create_table "products", id: :serial, force: :cascade do |t|
    t.string "product_name"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.integer "disipline_id"
    t.index ["disipline_id"], name: "index_products_on_disipline_id"
  end

  create_table "referensi_items", id: :serial, force: :cascade do |t|
    t.string "name"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
  end

  create_table "role_assignments", id: :serial, force: :cascade do |t|
    t.integer "user_id"
    t.integer "role_id"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.index ["role_id"], name: "index_role_assignments_on_role_id"
    t.index ["user_id"], name: "index_role_assignments_on_user_id"
  end

  create_table "roles", id: :serial, force: :cascade do |t|
    t.string "name", null: false
    t.text "permissions"
    t.string "type", null: false
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
  end

  create_table "types", id: :serial, force: :cascade do |t|
    t.integer "disipline_id"
    t.string "name_type"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.integer "product_id"
    t.index ["disipline_id"], name: "index_types_on_disipline_id"
    t.index ["product_id"], name: "index_types_on_product_id"
  end

  create_table "units", id: :serial, force: :cascade do |t|
    t.string "unit_name"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
  end

  create_table "user_activities", id: :serial, force: :cascade do |t|
    t.string "user_name"
    t.string "role"
    t.datetime "date", precision: 6
    t.string "description"
    t.string "controller"
    t.string "action"
    t.string "browser"
    t.string "ip_address"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
  end

  create_table "users", id: :serial, force: :cascade do |t|
    t.string "email", default: "", null: false
    t.string "encrypted_password", default: "", null: false
    t.string "reset_password_token"
    t.datetime "reset_password_sent_at", precision: 6
    t.datetime "remember_created_at", precision: 6
    t.integer "sign_in_count", default: 0, null: false
    t.datetime "current_sign_in_at", precision: 6
    t.datetime "last_sign_in_at", precision: 6
    t.string "current_sign_in_ip"
    t.string "last_sign_in_ip"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.string "user_type"
    t.string "name"
    t.string "username"
    t.index ["email"], name: "index_users_on_email", unique: true
    t.index ["reset_password_token"], name: "index_users_on_reset_password_token", unique: true
  end

  create_table "vendors", id: :serial, force: :cascade do |t|
    t.string "name"
    t.integer "country_id"
    t.string "business_category"
    t.string "address"
    t.string "website"
    t.string "email"
    t.string "phone_number"
    t.string "pic1"
    t.string "position1"
    t.string "email1"
    t.string "phone1"
    t.string "pic2"
    t.string "position2"
    t.string "email2"
    t.string "phone2"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
  end

  add_foreign_key "brochures", "items"
  add_foreign_key "delivery_items", "deliveries"
  add_foreign_key "delivery_items", "price_items"
  add_foreign_key "detail_items", "items", on_delete: :cascade
  add_foreign_key "detail_items", "units", on_delete: :cascade
  add_foreign_key "indonesia_cities", "indonesia_provinces", column: "province_id", name: "indonesia_provinces"
  add_foreign_key "indonesia_districts", "indonesia_cities", column: "city_id", name: "indonesia_cities"
  add_foreign_key "indonesia_villages", "indonesia_districts", column: "district_id", name: "indonesia_districts"
  add_foreign_key "item_price_letters", "items"
  add_foreign_key "item_price_letters", "price_items"
  add_foreign_key "items", "brands", on_delete: :cascade
  add_foreign_key "items", "disiplines", on_delete: :cascade
  add_foreign_key "items", "products", on_delete: :cascade
  add_foreign_key "items", "types", on_delete: :cascade
  add_foreign_key "items", "vendors"
  add_foreign_key "price_items", "countries", on_delete: :cascade
  add_foreign_key "price_items", "currencies", on_delete: :cascade
  add_foreign_key "price_items", "items", on_delete: :cascade
  add_foreign_key "price_items", "price_letters", on_delete: :cascade
  add_foreign_key "price_items", "referensi_items", on_delete: :cascade
  add_foreign_key "price_items", "vendors", on_delete: :cascade
  add_foreign_key "products", "disiplines", on_delete: :cascade
  add_foreign_key "role_assignments", "roles", on_update: :cascade, on_delete: :cascade
  add_foreign_key "role_assignments", "users", on_update: :cascade, on_delete: :cascade
  add_foreign_key "types", "disiplines", on_delete: :cascade
  add_foreign_key "types", "products", on_delete: :cascade
  add_foreign_key "vendors", "countries", name: "countries"
end
